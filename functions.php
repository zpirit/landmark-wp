<?php
function my_theme_enqueue_styles() {

    $parent_style = 'bento-style';

    wp_enqueue_style( $parent_style, get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'child-style',
        get_stylesheet_directory_uri() . '/style.css',
        array( $parent_style ),
        wp_get_theme()->get('Version')
    );
    wp_enqueue_style( 'flexslider-custom-css', get_stylesheet_directory_uri() . '/js/libs/flexslider-2.6.3/flexslider.css' );
}
add_action( 'wp_enqueue_scripts', 'my_theme_enqueue_styles' );

/**
 * Registers an editor stylesheet for the theme.
 */
function nmec_theme_add_editor_styles() {
    add_editor_style( 'editor-styles.css' );
}
add_action( 'admin_init', 'nmec_theme_add_editor_styles' );

// hook in late to make sure the parent theme's registration 
// has fired so you can undo it. Otherwise the parent will simply
// enqueue its script anyway.
add_action('wp_enqueue_scripts', 'wpse26822_script_fix', 900);
function wpse26822_script_fix()
{
    wp_enqueue_script('bento-child-script', get_stylesheet_directory_uri().'/js/script.min.js', array(), '20180707', true );
}

//function insert_jquery(){
//wp_enqueue_script('jquery', false, array(), false, false);
//}
//add_filter('wp_enqueue_scripts','insert_jquery',1);

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/
require_once( get_stylesheet_directory() . '/inc/zp_bruteforce_protection.php'); // Zpirit Bruteforce protection
require_once( get_stylesheet_directory() . '/inc/zp_social_meta_tags.php'); // Add Open graph tags to wp head
require_once( get_stylesheet_directory() . '/inc/zp_admin_area/zp_admin_area.php'); // Zpirit for admin area
require_once( get_stylesheet_directory() . '/inc/zp_seo_description.php'); // SEO
require_once( get_stylesheet_directory() . '/inc/zp_create_custom_post_type.php'); // its in the name dude.
require_once( get_stylesheet_directory() . '/inc/zp_create_custom_taxonomy.php'); // its in the name dude.


/* Tilsette og kontaktpunkt */
$ansatt_post_type = new ZP_create_custom_post_type( 'ansatt', 'ansatt', 'Ansatt', 'Ansatte', TRUE, 25.5650, 'dashicons-id-alt', array('title', 'thumbnail') );
$ansatt_taxonomy_department = new ZP_create_custom_taxonomy( 'avdeling', 'ansatt', 'avdeling', 'Avdeling', 'Avdelinger', TRUE );

/* Referanser og prosjekt */
$referanser_post_type = new ZP_create_custom_post_type( 'lm-referanser', 'lm-referanser', 'Referanse', 'Referanseprosjekt', TRUE, 25.5650, 'dashicons-id-alt', array('title', 'thumbnail', 'editor') );
$referanser_taxonomy_department = new ZP_create_custom_taxonomy( 'referanser-segment', 'lm-referanser', 'segment', 'Segment', 'Segment', TRUE );



function child_theme_setup() {
 	add_theme_support( 'post-thumbnails' );
    add_image_size( 'employee-thumb', 300, 350, true );
    add_image_size( 'fivexthree', 500, 300, true );

 }
 add_action( 'after_setup_theme', 'child_theme_setup' );


// Forandre namn og icon på menyelementet Posts til Artikler

function change_post_menu_label() {
	global $menu;
	global $submenu;
	$menu[5][0] = 'Artikler';
	$submenu['edit.php'][5][0] = 'Artikler';
	$submenu['edit.php'][10][0] = 'Legg til artikkel';
	//$submenu['edit.php'][15][0] = 'Kategoriar'; // Change name for categories
	//$submenu['edit.php'][16][0] = 'Merkelappar'; // Change name for tags
	echo '';
}

function change_post_object_label() {
		global $wp_post_types;
		$labels = &$wp_post_types['post']->labels;
		$labels->name = 'Artikler';
		$labels->singular_name = 'Artikkel';
		$labels->add_new = 'Legg til artikkel';
		$labels->add_new_item = 'Legg til artikkel';
		$labels->edit_item = 'Endre artikkel';
		$labels->new_item = 'Artikkel';
		$labels->view_item = 'Vis artikkel';
		$labels->search_items = 'Søk artikler';
		$labels->not_found = 'Ingen artikler funne';
		$labels->not_found_in_trash = 'Ingen artikler funne i papirkorga';
	}
add_action( 'init', 'change_post_object_label' );
add_action( 'admin_menu', 'change_post_menu_label' );

function et_menu_news_icon() {
  global $menu;
  foreach ( $menu as $key => $val ) {
	if ( __( 'Artikler') == $val[0] ) {
	  $menu[$key][6] = 'dashicons-welcome-write-blog';
	}
  }
}
add_action( 'admin_menu', 'et_menu_news_icon' );

// Utdrag på sider
add_post_type_support( 'page', 'excerpt' );