<?php // The default template for displaying pages ?>

<?php get_header(); ?>

<?php 
// Define variables
$bento_parent_page_id = get_the_ID();
?>

<?php $images = get_field('hero-image');
if( $images ): ?>
<div id="frontpageslider" class="flexslider">

    <ul class="slides">
        <?php foreach( $images as $image ): ?>
        <li style="background-image: url(<?php echo $image['url'] ?>)">
        	<div class="bnt-container">
				<header class="entry-header">
					<h2 class="entry-title"><?php the_field('hero-title'); ?><?php if( get_field('hero-button') ): ?><p>
					<a href="<?php the_field('hero-link'); ?>"><button class="hero-button"><?php the_field('hero-button'); ?></button></a></p>
					<?php endif; ?></h2>
					
				</header><!-- .entry-header -->
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>
<?php endif; ?>

<main class="site-main">
	<div class="fw-container frontal-intro" style="z-index: 2!important;">
 		<div class="bnt-container blacktone">
			<?php the_field('frontal-intro-text'); ?>
		</div>
	</div>


	<div class="fw-container services-wrapper">
		<div class="bnt-container services">
		    
		    
	   		<?php if( have_rows('services_boksar') ): ?>
			
			<ul>
			<?php while( have_rows('services_boksar') ): the_row(); 
		
				// vars
					$image = get_sub_field('image');
					$title = get_sub_field('title');
					$content = get_sub_field('content');
					$link = get_sub_field('link');
				?>
				<li class="singelboks grid25">
					<a href="<?php echo $link; ?>">
					<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
					<h3><?php echo $title; ?></h3>
					<?php echo $content; ?>
					</a>
				</li>
		
				
				
			<?php endwhile; ?>

			</ul>
			<?php endif; ?>

		</div></div>

	<div class="fw-container news-wrapper">
		<div class="bnt-container news">

				<?php $custom_query = new WP_Query('posts_per_page=3'); // limit number of posts
				while($custom_query->have_posts()) : $custom_query->the_post(); ?>
			
				<div <?php post_class('news-item grid33 nopadd'); ?> id="post-<?php the_ID(); ?>">					
					<?php
					// Must be inside a loop.
					 
					if ( has_post_thumbnail() ) {
					    the_post_thumbnail('fivexthree');
					}
					else {
					    echo '<img src="' . get_bloginfo( 'stylesheet_directory' ) 
					        . '/img/fivexthree-default.jpg" />';
					}
					?>
					
					
					<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
					<?php the_excerpt(); ?>
				</div>
			
				<?php endwhile; ?>
				<?php wp_reset_postdata(); // reset the query ?>	
			
			<div class="clearfix aligncenter">
				<a href="<?php echo get_page_link(15622); ?>" class="btn orange">Nyhetsarkiv</a>
			</div>
			
			</div>	
	</div>

</main>
<?php get_footer(); ?>
