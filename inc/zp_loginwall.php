<?php
/**
 * Zpirit Login Wall
 *
 * Add common spammer blocks and google captcha v3.
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 * @version 0.1
 */

class ZP_loginwall {
	private $version;
	private $site_key;
	private $secret_key;
	
	public function init() {
		$this->version = "0.5";
		$this->site_key = '6LeFu6oUAAAAAG5eU_rZY9yB7YydqZeSxtW0dOke';
		$this->secret_key = '6LeFu6oUAAAAAC9qVYag3CzyFzJFpGfWjG_p4ipi';

		// Delay wp-login.php load
		add_action( 'login_head', array( $this, 'zp_login_delay') );
		// Add login form fields
		add_action( 'login_form', array( $this, 'zp_add_login_field' ) );
		// Add the catpcha scripts to login page
		add_action( 'login_enqueue_scripts', array( $this, 'zp_enqueue_login_scripts') );
		// Ajax handler for catpcha validation
		add_action( 'wp_ajax_nopriv_zp_verify_gc', array( $this, 'zp_verify_gc') );
		add_action( 'wp_ajax_zp_verify_gc', array( $this, 'zp_verify_gc') );
		// Check form fields and captcha value
		add_action( 'wp_authenticate', array( $this, 'zp_check_captcha_score' ), 10, 2 );
		// add to low captcha score message
		add_filter( 'login_message', array( $this, 'zp_low_catpcha_message') );

	}

	/**
	 * Delay loading wp-login.php page
	 * to give the finger to scriptskiddies
	 */
	public function zp_login_delay() {
		// Take a nap
		sleep(3);
	}

	/**
	 * Add login form fields after password field
	 */
	function zp_add_login_field() {
		// add form fields
		$fields = array(
			'<input type="hidden" name="user-ip" value="'. $_SERVER['REMOTE_ADDR'] .'">',
			'<input type="hidden" name="user-weight" value="0.0">'
		);
		
		echo implode('', $fields);
	}

	/**
	 * Add scripts to login form
	 */
	public function zp_enqueue_login_scripts() {
		wp_enqueue_script( 'google-recaptcha', 'https://www.google.com/recaptcha/api.js?render='. $this->site_key, array(), $this->version, true );
		wp_enqueue_script( 'zp-loginwall', get_stylesheet_directory_uri() . '/inc/zp_loginwall.js', array('google-recaptcha'), $this->version, true );

		wp_localize_script( 'zp-loginwall', 'zpwall', array( 
			'ajax' => admin_url( 'admin-ajax.php' ), 
			'rand' => base64_encode($this->site_key), 
			'veritas' => wp_create_nonce( 'wallcall' ) 
		) );
	}

	/**
	 * Verify captcha token from client side
	 */
	public function zp_verify_gc() {
		
		// verify nonce
		if ( ! wp_verify_nonce( $_POST['veritas'], 'wallcall' ) ) 
			wp_send_json_error();

		// copy array and remove wp action name and nonce
		$query = $_POST;
		unset($query['action']);
		unset($query['veritas']);
		$query['secret'] = $this->secret_key;

		// Setup a curl handler
		$ch = curl_init();
		// curl options
		curl_setopt_array( 
			$ch,
			array(
				CURLOPT_URL  => 'https://www.google.com/recaptcha/api/siteverify',
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $query,
				CURLOPT_RETURNTRANSFER => true
			)
		);

		$data = curl_exec( $ch );
		
		if ( curl_errno( $ch ) ) {
			wp_send_json_error( array( curl_error( $ch ) ) );
		}

		curl_close($ch);

		// Set a session score and return success if we got a result
		$data = json_decode($data);
		if ( !empty($data) && !empty($data->success) && $data->success === true ) {
			session_start();
			$_SESSION['user-score'] = round( $data->score, 1 );
			wp_send_json_success( $data );
		}

		// all else failed
		wp_send_json_error();
	}

	/**
	 * Validate the user catcha score
	 */
	public function zp_check_captcha_score( $user, $password) {		
		
		if ( empty($user) && empty($password) )
			return;

		// session to get captcha score
		session_start();
		
		// No session data
		if ( empty( $_SESSION['user-score'] ) ) {
			wp_redirect( wp_login_url(), 200 );
			exit;
		}
		// No session data of the user-score
		else if ( $_SESSION['user-score'] < 0.5 ) {
			wp_redirect( wp_login_url() . '?score='.$_SESSION['user-score'], 302 );
			exit;
		}
	}

	/**
	 * Catpcha score to low lets warn the user
	 */
	public function zp_low_catpcha_message( $messages ) {

		// user catpcha score to low < 0.5
		if( ! empty($_GET) && ! empty($_GET['score']) ) {
			$messages .= '<div id="login_error"><strong>Oi då: </strong>Du er vist ein robot.</div>';
		}

		return $messages;
	}

}

/**
 * Init the class
 */
add_action( 'init', function() { 
		global $zp_loginwall;
		$zp_loginwall = new ZP_loginwall();
		$zp_loginwall->init();
	}
);