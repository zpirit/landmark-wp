<?php
/**
 * Zpirit Bruteforce Protection
 *
 * Bruteforce protection for wordpress by zpirit.
 *
 * A class that blocks ip adress if serveral failed 
 * login attempts comming from same ip.
 *
 * Changelog: 1.7 - added time of bruteforce attempt to email template
 * Changelog: 1.7.5 - Changed capture method of password to REQUEST.
 * Changelog: 1.8 - Using $max_login_attempts to compare bad logins attempts from db.
 * Changelog: 1.8 - Whitelist of ip, localhost and zpirit office ip
 * Changelog: 1.9 - No more log table, do all the work in one table.
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 * @version 1.9
 */

// initiate class
$zp_bruteforce = new ZP_bruteforce_protection;
$zp_bruteforce->init();

class ZP_bruteforce_protection {
	private $max_login_attempts = 3;
	private $bann_time = 86400; // 24 Hours
	private $banmsg = "Your ip address i blocked.";
	private $sqltable, $wpdb, $user_ip;
	// private $whitelist = ['::1', '127.0.0.1', '10.0.0.150', '89.10.113.250'];
	private $whitelist = array('89.10.113.250');

	public function init() {
		global $wpdb;
		$this->wpdb = &$wpdb;
		$this->sqltable = $this->wpdb->prefix . 'zp_bruteforce';
		$this->user_ip = $_SERVER['REMOTE_ADDR'];

		add_action( 'wp_login_failed', array( $this, 'zp_insert_record_if_login_error'));
		add_action( 'init', array( $this, 'zp_redirect_if_bruteforcer') );
		add_filter( 'login_message', array( $this, 'zp_login_attempts_left') );
		add_action( 'wp_login', array( $this, 'zp_clear_bruterforcer_entry') );
		add_filter( 'login_errors', array( $this, 'zp_change_wrong_user_pass_error_message' ) );

		// Create tables if not already there
		if($this->wpdb->get_var("SHOW TABLES LIKE '{$this->sqltable}'") != $this->sqltable) {
			$create_sqltable_sql = "CREATE TABLE IF NOT EXISTS {$this->sqltable} (
										`id` int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
										`username` varchar(75) NULL,
										`attempt` int(2) NULL,	
										`ip` varchar(20) NULL,
										`banned` tinyint(1) NULL DEFAULT 0,
										`tried_at` int(20) NULL
									) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci";
			$this->wpdb->query($create_sqltable_sql);
		}
	}

	// Get latest entry in database
	public function zp_get_last_row() {
		$last_row = $this->wpdb->get_row("SELECT * FROM {$this->sqltable} WHERE ip='{$this->user_ip}' ORDER BY id DESC LIMIT 1");
		// return with the latest entry for ip.
		return $last_row;
	}

	// Send forbidden header
	public function zp_execute_order_66() {
		header("HTTP/1.1 403 Forbidden");
		header("Connection: close");
		die($this->banmsg);
	}
	
	// reset attempts on successfull login
	public function zp_clear_bruterforcer_entry() {
		// clear user ip from bruteforce table, but not from log on successfull login
		$update = array('attempt' => 0, 'tried_at' => time());
		$where = array('ip' => $this->user_ip);
		$this->wpdb->update($this->sqltable, $update, $where);
	}

	public function zp_send_email_notification_to_support( $username, $passwd, $blockaction ) {
		// Send email to admin setup
		$zp_bruteforcer_emailto = 'support@zpirit.no';
		$zp_bruteforcer_subject = get_bloginfo( 'blogname' ) . ' - BRUTEFORCE ANGREP';
		$zp_bruteforcer_headers = array('MIME-Version: 1.0','From: Zpirit Zupport <noreply@zpirit.no>', 'Content-Type: text/html; charset=UTF-8');
		
		// Email template array
		$zp_bruteforcer_html5mail_array = array(
			'<!DOCTYPE HTML>',
			'<html lang="nn">',
			'<head>',
				'<title>Bruteforce angrep på nettsida di</title>',
				'<meta charset="UTF-8">',
			'</head>',
			'<body style="background:#eee; margin:0; padding:50px 0;">',
			'<div style="width:560px; background:#fff; border-radius:12px; padding:20px 20px; margin:0 auto;">',			
			'<h1 style="text-align:center;"><a href="http://zpirit.no/"><img src="http://zpirit.no/wp-content/themes/zpiritreklame/images/zpirit-logo-red.png" alt="Zpirit"></a></h1>',
			'<h3 style="text-align:center; text-transform:uppercase; margin:2em 0;">Bruteforce angrep på nettsida di <a href="'. get_home_url() .'" style="color:#BE0928; text-decoration:none; font-weight:bold;">'. get_bloginfo('blogname') .'</a></h3>',
			'<p style="margin:2em 0;">Nytt bruteforce angrep har blitt stogga på wp-login.php skjema på nettsida. Angripar blir blokkert i 24 timar med 403 header vist det er fyrste gong han prøvar seg, er det andre forsøk så bli ip adressa hans permanent blokkert i .htaccess fila.</p>',
			'<table border="0" cellpadding="10" cellspacing="0" style="width: 100%; color:#454545; border-collapse:collapse;">',
			'<thead>',
			'<tr>',
			'<th style="background:#BE0928; height:40px; color:#fff; border: 1px solid ##BE0928; border-radius:10px 10px 0px 0px;" colspan="2">Angrep Blokkert</th>',
			'</tr>',
			'</thead>',
			'<tbody>',
			'<tr style="height:50px;">',
			'<td style="width:30%; border: 1px solid #ccc;">IP</td>',
			'<td style="width:70%; text-align:right; border: 1px solid #ccc;"><a href="https://who.is/whois-ip/ip-address/'. $this->user_ip .'" style="color:#BE0928; text-decoration:none; font-weight:bold;">'. $this->user_ip .'</a></td>',
			'</tr>',
			'<tr style="height:50px;">',
			'<td style="width:30%; border: 1px solid #ccc;">Brukarnamn</td>',
			'<td style="width:70%; text-align:right; border: 1px solid #ccc;">'. $username .'</td>',
			'</tr>',
			'<tr style="height:50px;">',
			'<td style="width:30%; border: 1px solid #ccc;">Passord</td>',
			'<td style="width:70%; text-align:right; border: 1px solid #ccc;">'. $passwd .'</td>',
			'</tr>',
			'<tr style="height:50px;">',
			'<td style="width:30%; border: 1px solid #ccc;">Angrep utført</td>',
			'<td style="width:70%; text-align:right; border: 1px solid #ccc;">'. date( 'd.F Y H:i' ) .'</td>',
			'</tr>',
			'<tr style="height:50px;">',
			'<td style="width:30%; border: 1px solid #ccc;">Blokkert til</td>',
			'<td style="width:70%; text-align:right; border: 1px solid #ccc;">'. date( 'd.F Y H:i', (time()+$this->bann_time) ) .'</td>',
			'</tr>',
			'<tr>',
			'<td valign="top" style="width:30%; border: 1px solid #ccc;">Handling</td>',
			'<td style="width:70%; font-style:italic; color:#737373; border: 1px solid #ccc;">'. $blockaction .'</td>',
			'</tr>',
			'</tbody>',
			'</table>',
			'<p style="text-align:center; font-size:0.8em; color:#777; margin:3em 0 0 0;">E-post design frå <a href="http://zpirit.no/" title="zpirit.no" style="color:#BE0928; text-decoration:none; font-weight:bold;">Zpirit Reklame</a></p>	',
			'</div>',
			'</body>',
			'</html>',
		);
		$zp_bruteforcer_html5mail = implode("", $zp_bruteforcer_html5mail_array);

		// SEND EMAIL TO ADMIN WITH INFO ABOUT ATTACKER AND BLOCK ACTION
		wp_mail( $zp_bruteforcer_emailto, $zp_bruteforcer_subject, $zp_bruteforcer_html5mail, $zp_bruteforcer_headers);
	}

	public function zp_insert_record_if_login_error($username) {
		// do nothing if the user ip is in whitelist
		if ( in_array( $this->user_ip, $this->whitelist ) )
			return;

		$have_failed = $this->zp_get_last_row();
		// user have attempts in database
		if($have_failed !== null) {
			$zp_pwd = isset($_REQUEST['pwd']) ? $_REQUEST['pwd'] : "uten-passord";

			// IP have already tried max_login_attempts times so this is the blocking attempt!
			if( $have_failed->attempt >= ($this->max_login_attempts - 1) ) {
				// and attempt to db
				$update = array('username' => $username, 'attempt' => $have_failed->attempt + 1, 'banned' => 1, 'tried_at' => time());
				$where = array('id' => $have_failed->id);
				$this->wpdb->update($this->sqltable, $update, $where);

				// User have already been blocked, so add him to permanent .htaccess block
				if ( $have_failed->banned === "1" ) {
					$zp_blockaction = "Ip adressa har blitt auto blokkert før.";
					// Add attacker to .htaccess file denied list
					$htaccess = ABSPATH . ".htaccess";
					$htdata = file_get_contents( $htaccess );
					// We found the htaccess file and it is not empty
					if( $htdata !== false && $htdata !== "" ) {
						// We found the ending bruteforce attempts tag in htaccess
						if( strpos( $htdata , "# END Bruteforce attempts" ) ) {
							// The atacker is not already blocked by the htaccess
							if( !strpos( $htdata, "deny from " . $this->user_ip ) ) {
								$newEntry = preg_replace( '/(\n#\sEND\sBruteforce\sattempts)/', "\ndeny from {$this->user_ip}$1", $htdata );
								// We have now added ip to htaccess file
								if( file_put_contents( $htaccess, $newEntry ) ) {
									// Send email to admin
									$zp_blockaction .= 'Og eg la til ip adressa til angriparen i .htaccess fila.</p>';
								}
								// Error writing to htaccess file
								else {
									// Send email to admin
									$zp_blockaction .= 'Men eg kunne ikkje skrive til htaccess fila for å permanent blokkera angriparen.';
								}
							}
							// Attacker is already added to the htaccess file
							else {
								// Send email to admin
								$zp_blockaction .= 'Dette burde jo vera umogleg? Angriparen er allereie blokkert i .htaccess fila.';
							}
						}
						// Found the htaccess file but could not find ending bruteforce tag to replace
						else {
							// Send email to admin
							$zp_blockaction .= 'Men eg kunne ikkje finna # END Bruteforce attempts i .htaccess fila for å blokkera han.';
						}
					}
					// Did not fint the htaccess file or the file is empty
					else {
						// Send email to admin
						$zp_blockaction .= 'Men eg fann ikkje .htaccess fila når eg skulle blokkera angriparen.';
					}

				}
				else {
					$zp_blockaction = 'Brukaren er utestengt for fyrste gong. Ingen .htaccess blokkering utført.';
				}

				// send email about block on ip.
				$this->zp_send_email_notification_to_support( $username, $zp_pwd, $zp_blockaction );

				// Send forbidden header
				$this->zp_execute_order_66();

			}
			// user have less than max_login_attempts bad logins
			else {
				// Increment user attempts
				$update = array('username' => $username, 'attempt' => $have_failed->attempt + 1, 'tried_at' => time());
				$where = array('id' => $have_failed->id);
				$this->wpdb->update($this->sqltable, $update, $where);

			}
		}
		else {
			// add first failed login attempt to database
			$values = array('username' => $username, 'attempt' => 1, 'ip' => $this->user_ip, 'tried_at' => time());
			$this->wpdb->insert($this->sqltable, $values);

		}
	}

	public function zp_redirect_if_bruteforcer() {
		global $pagenow;
		if( "wp-login.php" == $pagenow ) {

			// Delete old attempts, older than 90 days
			// $this->wpdb->query("DELETE FROM ". $this->sqltable ." WHERE tried_at < UNIX_TIMESTAMP(DATE_SUB(NOW(), INTERVAL 90 DAY))");

			$wrongdoer = $this->zp_get_last_row();
			if( $wrongdoer !== null && $wrongdoer->attempt != 0 ) {
				if( $wrongdoer->attempt >= $this->max_login_attempts && ($wrongdoer->tried_at + $this->bann_time) > time() ) {
					// wrongdoer is locket out and is still under ban time
					// Send forbidden header
					$this->zp_execute_order_66();
				}
				elseif( $wrongdoer->attempt >= $this->max_login_attempts && ($wrongdoer->tried_at + $this->bann_time) < time()) {
					// wrongdoer has beeen locked out but is past ban time
					$this->zp_clear_bruterforcer_entry();
				}
				elseif ( $wrongdoer->attempt < $this->max_login_attempts &&  ($wrongdoer->tried_at + $this->bann_time) < time() ) {
					// Wrongdoer has bad login attempts but tried for longer than ban time ago
					$this->zp_clear_bruterforcer_entry();
				}
			}

		}
	}

	public function zp_login_attempts_left() {
		// do nothing if the user ip is in whitelist
		if ( in_array( $this->user_ip, $this->whitelist ) ) {
			$url = parse_url( get_site_url() );
			$message = '<div class="message">Hei, kjekt å sjå deg igjen!<br>Logg inn i skjemaet nedanfor og lurar du på noko så spør <a href="mailto:support@zpirit.no?subject=Problem med logg inn på '. $url['host'] .'">support@zpirit.no</a> om hjelp.</div>';
				
			return $message;
		}

		// get row from bruteforce table with visitor ip
		$have_attempted = $this->zp_get_last_row();
		// user have a failed login
		if( $have_attempted !== null && $have_attempted->attempt != 0 ) {
			// if user is on the 4 attempt, show last warning
			if($have_attempted->attempt > ($this->max_login_attempts - 2) ) {
				$waitleft = (time() - $have_attempted->tried_at) / 60;
				$waitleft = ($this->bann_time / 60) - round($waitleft);

				$message = '<div id="login_error">Du har <span style="color: red; font-wheight: 700;">'. ($this->max_login_attempts - $have_attempted->attempt) .'</span> login forsøk igjen<br><br>Vent <span style="color: red; font-wheight: 700;">'. $waitleft .'</span>min for å nullstille.</div>';
				
				return $message;
			}
			// user have less than 4 attempts, show count laft
			else {
				$message = '<div id="login_error">Du har <span style="color: red; font-wheight: 700;">'. ($this->max_login_attempts - $have_attempted->attempt) .'</span> login forsøk igjen.</div>';
				return $message;
			}
		}
		// no failed login atttempts
		return;
	}
	public function zp_change_wrong_user_pass_error_message( $error ) {
		// Set common failed login meessage to hide correct username verification
		$error = "<strong>OBS:</strong> Feil brukernavn eller passord.<br>";

		return $error;
	}
}
