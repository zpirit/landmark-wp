<?php
/**
 *
 * Add social opengraph tags to html head
 *
 * Enjoy!!!
 *
 * @author Øyvind eikeland <oyvind@zpirit.no>
 * @since 1.0
 */

add_action('wp_head', 'zp_social_meta_tags', 5);

function zp_social_meta_tags() {
	global $post;
	
	if(is_single() || is_page() && !is_front_page()) :
	
		setup_postdata($post);
		
			if( has_post_thumbnail($post->ID) ) {
				$image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'full');
				$image = $image[0];
			}
			$link = get_permalink($post->ID);
			$title = get_the_title($post->ID);
			$excerpt = get_the_excerpt();
		?>
		
		<!-- Open Graph data -->
		<meta property="og:url" content="<?php echo $link; ?>" />
		<meta property="og:type" content="article" />
		<meta property="og:title" content="<?php echo $title; ?>" />
		<meta property="og:description" content="<?php echo strlen(strip_tags($excerpt)) == 0 ? bloginfo('description') : strip_tags($excerpt); ?>" />
		<?php if( isset($image) ) : ?>
			<meta property="og:image" content="<?php echo $image; ?>" />
		<?php else : ?>
			<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" />
		<?php endif; ?>
		<meta property="og:site_name" content="<?php echo get_bloginfo( 'name' ); ?>" />
		<?php
		wp_reset_postdata();
	else :
		?>
		<!-- Open Graph data -->
		<meta property="og:url" content="<?php echo site_url(); ?>" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="<?php echo get_bloginfo( 'name' ); ?> — <?php bloginfo('description'); ?>" />
		<meta property="og:description" content="Landmark Maskin er Sunnhordlands ledende grunnarbeidsentreprenør. Vi utfører kvalitetsarbeid uten ubehagelige overraskelser som fordyrer prosjektet." />
		<meta property="og:image" content="<?php echo get_stylesheet_directory_uri(); ?>/screenshot.png" />
		<meta property="og:site_name" content="<?php echo get_bloginfo( 'name' ); ?>" />
		<?php
	
	endif;

}
?>