<?php
/**
 * Class ZP_create_custom_taxonomy
 * Register a custom taxonomy.
 *
 * Class for creating new custom taxonomy.
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 * @since 1.3
 */
class ZP_create_custom_taxonomy {
	public $slug, $post_type, $permalink, $title_singular, $title_plural, $hierarchical;

	/**
	 * Class construct
	 * @param string $slug           The taxonomy slug.
	 * @param array  $post_type      The post type this tax belongs to.
	 * @param string $permalink      The pretty permalink to txonomy.
	 * @param string $title_singular The singular title of tax in admin area.
	 * @param string $title_plural   The plural title of tax in admin area.
	 * @param bol    $hierarchical   Like category or like tag.
	 */
	public function __construct($slug, $post_type, $permalink, $title_singular, $title_plural, $hierarchical) {
		$this->slug = $slug;
		$this->post_type = $post_type;
		$this->permalink = _x($permalink, 'URL slug', 'first-child');
		$this->title_singular = $title_singular;
		$this->title_plural = $title_plural;
		$this->hierarchical = $hierarchical;

		add_action( 'init', array($this, 'zp_register_taxonomy'), 0 );		

	}

	/**
	 * Generate the labels for tax
	 * @return array All the labels for tax
	 */
	public function zp_generate_labels() {
		$themeinfo = wp_get_theme();
		$textdomain = $themeinfo->get( 'TextDomain' );
		$labels = array(
			'name'              => $this->title_plural,
			'singular_name'     => $this->title_singular,
			'search_items'      => __( 'Search in ', $textdomain ) . strtolower($this->title_plural),
			'all_items'         => __( 'All ', $textdomain ) . strtolower($this->title_plural),
			'parent_item'       => __( 'Parent', $textdomain ),
			'parent_item_colon' => __( 'Parent:', $textdomain ),
			'edit_item'         => __( 'Edit', $textdomain ),
			'update_item'       => __( 'Update', $textdomain ),
			'add_new_item'      => __( 'Create new ', $textdomain ) . strtolower($this->title_singular),
			'new_item_name'     => __( 'New ', $textdomain ) . strtolower($this->title_singular),
			'menu_name'         => $this->title_plural
		);

		return $labels;
	}

	/**
	 * Setup args for tax
	 * @return array Generate arguments for setting up taxonomy with labels
	 */
	public function zp_build_args() {
		$args = array(
			'hierarchical'      => $this->hierarchical,
			'labels'            => $this->zp_generate_labels(),
			'show_ui'           => true,
			'show_in_rest'      => true,
			'show_in_nav_menus' => true,
			'show_tagcloud'		=> false,
			'show_admin_column' => true,
			'query_var'         => $this->permalink,
			'rewrite'           => array( 'slug' => $this->permalink )
		);

		return $args;
	}

	/**
	 * Register the taxonomy
	 * @return null
	 */
	public function zp_register_taxonomy() {
		register_taxonomy( $this->slug, $this->post_type, $this->zp_build_args() );
		// flush_rewrite_rules();
	}
}
?>