<?php
/**
 * Class ZP_create_custom_post_type
 * Register a custom post type.
 *
 * Class for creating new custom post types.
 *
 * Enjoy!!!
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 * @since 1.0
 */

class ZP_create_custom_post_type {
	public $slug, $permalink, $title_single, $title_plural, $frontside, $position, $menu_icon, $supports;

	/**
	 * __construct for setting up post type parameters and arguments
	 *
	 * @param string $slug         The post type slug
	 * @param string $permalink    The pretty permalink to the post type.
	 * @param string $title_single The SINGULAR title of the post type for admin area.
	 * @param string $title_plural The PLURAL title of post type for admin area.
	 * @param bolean $frontside    Should this post be accessible on the front page
	 * @param int    $position     Admin menu position of post type see: https://codex.wordpress.org/Function_Reference/register_post_type#menu_position
	 * @param string $menu_icon    Admin menu icon of post type see: https://developer.wordpress.org/resource/dashicons/
	 * @param array  $supports     post type options: 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
	 */
	public function __construct($slug, $permalink, $title_single, $title_plural, $frontside, $position, $menu_icon, $supports) {
		$this->slug = $slug;
		$this->permalink = $permalink;
		$this->title_single = $title_single;
		$this->title_plural = $title_plural;
		$this->frontside = $frontside;
		$this->position = $position;
		$this->menu_icon = $menu_icon;
		$this->supports = $supports;

		add_action( 'init', array($this, 'zp_add_custom_post_type'));
	}

	/**
	 * Build labels for post type
	 * @return array Labels for post type
	 */
	public function zp_build_labels() {
		$labels = array(
				'name'               => $this->title_plural,
				'singular_name'      => $this->title_single,
				'menu_name'          => $this->title_plural,
				'name_admin_bar'     => $this->title_single,
				'add_new'            => __('Create new', 'first-child'),
				'add_new_item'       => sprintf(__('Create new %s', 'first-child'), strtolower($this->title_single)),
				'new_item'           => sprintf(__('New %s', 'first-child'), strtolower($this->title_single)),
				'edit_item'          => sprintf(__('Edit %s', 'first-child'), strtolower($this->title_single)),
				'view_item'          => sprintf(__('Show %s', 'first-child'), strtolower($this->title_single)),
				'all_items'          => sprintf(__('All %s', 'first-child'), strtolower($this->title_plural)),
				'search_items'       => sprintf(__('Search %s', 'first-child'), strtolower($this->title_plural)),
				'parent_item_colon'  => sprintf(__('Parent %s', 'first-child'), strtolower($this->title_single)),
				'not_found'          => sprintf(__('Found no %s', 'first-child'), strtolower($this->title_plural)),
				'not_found_in_trash' => sprintf(__('Found no %s in trash can', 'first-child'), strtolower($this->title_plural))
			);
		// return complete lebels array
		return $labels;
	}

	/**
	 * Setup arguments array
	 * @return array All the args with labels
	 */
	public function zp_build_args() {
		$args = array();
		$args['labels'] = $this->zp_build_labels();
		$args['description'] = "";
		if($this->frontside == false) {
			$args['public'] = false;
			$args['exclude_from_search'] = true;
			$args['publicly_queryable'] = false;
			$args['show_ui'] = true;
			$args['show_in_nav_menus'] = false;
			$args['has_archive'] = false;
		}
		else {
			$args['public'] = true;
			$args['publicly_queryable'] = true;
			$args['has_archive'] = true;
			$args['show_in_rest'] = true;
		}
		$args['menu_position'] = $this->position;
		$args['menu_icon'] = $this->menu_icon;
		$args['hierarchical'] = in_array( 'page-attributes', $this->supports ) ? true : false;
		$args['supports'] = $this->supports;
		$args['can_export'] = true;
		$args['rewrite'] = array('slug' => $this->permalink); 
		$args['query_var'] = $this->permalink;

		return $args;
	}

	/**
	 * Register the custom post type. Called from construct
	 * @return null
	 */
	public function zp_add_custom_post_type() {
		register_post_type( $this->slug, $this->zp_build_args() );
	}

	/**
	 * Flush rewrite rules
	 */
	public function zp_flush_the_toilet() {
		add_action('init', array( $this, 'zp_do_flush_the_toilet'));
	}
	/**
	 * do the flushing
	 */
	public function zp_do_flush_the_toilet() {
		flush_rewrite_rules();
	}
}
?>