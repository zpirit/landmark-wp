<?php
/**
 * Change admin area stuff
 *
 * @author Øyvind Eikeland <oyvind@zpirit.no>
 */

// Add style to wp-login.php page
add_action( 'login_enqueue_scripts', 'zp_admin_styles' );
add_action( 'admin_enqueue_scripts', 'zp_admin_styles' );
function zp_admin_styles( $hook ) {
	wp_register_style( 'zp_admin_stylesheet', get_stylesheet_directory_uri() . '/inc/zp_admin_area/zp_admin_area.css', false, '1.0' );
	wp_enqueue_style( 'zp_admin_stylesheet' );

	// Admin script
	wp_enqueue_script( 'zp_admin_area_script', get_stylesheet_directory_uri() . '/inc/zp_admin_area/zp_admin_area.js', array('jquery'), '1.0.3' );
	
	if( in_array( $hook, array("post.php", "post-new.php") ) ) {
		$screen = get_current_screen();
	}
}

// Change wp-login.php page logo url
add_filter( 'login_headerurl', 'zp_login_logo_url' );
function zp_login_logo_url() {
	return "http://zpirit.no";
}

// Change title attribute for wp-login.php logo link
add_filter( 'login_headertitle', 'zp_login_logo_url_title' );
function zp_login_logo_url_title() {
	return 'Zpirit Reklamebyrå';
}

// Add favicon to login and admin area
add_action('login_head', 'zp_admin_add_favicon');
add_action('admin_head', 'zp_admin_add_favicon');
function zp_admin_add_favicon() {
	echo '<link rel="icon" type="image/png" href="' . get_stylesheet_directory_uri() . '/img/icons/favicon.png" />';
}

/**
 * REMOVE X-PINGBACK APACHE WORDPRESS HEADER
 */
add_filter('wp_headers', 'zpirit_remove_x_pingback');
function zpirit_remove_x_pingback($headers) {

    unset($headers['X-Pingback']);

    return $headers;
}


/**
 * SEQURITY: remove stupid standard login redirects
 */
add_action( 'template_redirect', function() {
	$req = untrailingslashit($_SERVER['REQUEST_URI']);
	if( site_url('login', 'relative') === $req ||
		site_url('admin', 'relative') === $req ||
		site_url('dashboard', 'relative') === $req ) {
			remove_action( 'template_redirect', 'wp_redirect_admin_locations', 1000 );
		}
	}
);

/**
 * Add Zpirit Support Dashboard Widget
 */
add_action('wp_dashboard_setup', 'zp_dashboard_widgets');
function zp_dashboard_widgets() {
	wp_add_dashboard_widget('zp_support_dash_widget', 'Zpirit support', 'zp_support_dashboard');
}
// Display for 'zp_support_dash_widget'
function zp_support_dashboard() {
	echo '<p>Står du fast? Kontakt gjerne support:</p>';
	echo '<p class="bigger"><strong>E-post: <a href="mailto:support@zpirit.no">support@zpirit.no</a></strong></p>';
	echo '<p class="bigger"><strong>Tlf: <a href="tel:91993461">919 93 461</a></strong></p>';
	echo '<p class="bigger"><strong>Nett: <a href="http://zpirit.no/">zpirit.no</a></strong></p>';
}

/**
 * Remove default dashboard widgets
 */
add_action( 'admin_init', 'zp_remove_dashboard_meta' );
function zp_remove_dashboard_meta() {
        remove_meta_box( 'dashboard_incoming_links', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_plugins', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_secondary', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_drafts', 'dashboard', 'side' );
        remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
        // remove_meta_box( 'dashboard_right_now', 'dashboard', 'normal' );
        remove_meta_box( 'dashboard_activity', 'dashboard', 'normal');
        remove_meta_box( 'cms_tpv_dashboard_widget_page', 'dashboard', 'normal');
}

/**
 * Change admin footer text
 */
add_filter('admin_footer_text', 'zp_change_footer_text');
function zp_change_footer_text() {
	echo sprintf(
		'Zpirit support - E-post <a href="mailto:%1$s">%1$s</a> - Telefon <a href="tel:%2$s">%2$s</a> - Ta gjerne kontakt med oss. <a href="%3$s"><img src="%4$s" alt="Zpirit Reklamebyrå" /></a>',
		'support@zpirit.no',
		'919 93 461',
		'http://zpirit.no',
		get_stylesheet_directory_uri() . '/img/zpirit_raud_logo.svg'
	);
}


/**
 * Edit admin menu
 */
add_action( 'admin_menu', 'zp_menu_page_removing', 999 );
function zp_menu_page_removing() {
	global $menu;
	global $submenu;

	// Rename menu items
	// $menu[5][0] = 'Artikkel'; // rename post to article
	// $menu[20][0] = 'Kapittel'; // rename post to article

	// remove top level pages
	// remove_menu_page( 'index.php' );                  //Dashboard
	// remove_menu_page( 'jetpack' );                    //Jetpack*
	// remove_menu_page( 'edit.php' );                   //Posts
	// remove_menu_page( 'upload.php' );                 //Media
	// remove_menu_page( 'edit.php?post_type=page' );    //Pages
	// remove_menu_page( 'edit-comments.php' );          //Comments
	// remove_menu_page( 'themes.php' );                 //Appearance
	// remove_menu_page( 'plugins.php' );                //Plugins
	// remove_menu_page( 'users.php' );                  //Users
	// remove_menu_page( 'tools.php' );                  //Tools
	// remove_menu_page( 'options-general.php' );        //Settings

	// remove sub level pages
	// remove_submenu_page( 'themes.php', 'widgets.php' );
	// remove_submenu_page( 'themes.php', 'nav-menus.php' );
	// unset( $submenu["themes.php"][6] );

	// add top level menu items
	// add_menu_page(
	// 	'Meny', // page title
	// 	'Meny', // menu title
	// 	'edit_theme_options', // capability
	// 	'nav-menus.php', // menu slug
	// 	'', // Output function
	// 	'dashicons-menu', // icon
	// 	'65.123' // menu position
	// );
	// add_menu_page(
	// 	'Widgets', // page title
	// 	'Widgets', // menu title
	// 	'edit_theme_options', // capability
	// 	'widgets.php', // menu slug
	// 	'', // Output function
	// 	'dashicons-tagcloud', // icon
	// 	'65.234' // menu position
	// );
	// add_menu_page(
	// 	'Tema', // page title
	// 	'Tema', // menu title
	// 	'edit_theme_options', // capability
	// 	'themes.php', // menu slug
	// 	'', // Output function
	// 	'dashicons-welcome-view-site', // icon
	// 	'65.345' // menu position
	// );
	// add_menu_page(
	// 	'Dummy', // page title
	// 	'Dummy', // menu title
	// 	'edit_posts', // capability
	// 	'zp-dummy-admin.php', // menu slug
	// 	'zp_dummy_admin_output', // Output function
	// 	'dashicons-admin-tools', // icon
	// 	'20.1' // menu position
	// );
}

/**
 * Register theme setting
 */
// add_action( 'admin_init', 'zp_register_theme_settings');
function zp_register_theme_settings() {
	add_settings_section('zp_theme_section', 'Tema valg for Hatlandsåsen', null, 'zp-theme-options');
	add_settings_field('ha_trigger_step_2', 'Aktiver byggestrinn 2', 'zp_display_step2_field', 'zp-theme-options', 'zp_theme_section');
	register_setting( 'zp-theme-options', 'ha_trigger_step_2' );
}
function zp_display_step2_field() {
	echo '<input type="checkbox" name="ha_trigger_step_2" id="ha_trigger_step_2" value="1" class="code" '. checked( 1, get_option( 'ha_trigger_step_2' ), false) .' />';
}
// Output for admin page zp-dummy-admin.php
function zp_theme_options_output() {
	?>
		<div class="wrap">
			<h1>Tema valg.</h1>

			<form method="post" action="options.php">
				<?php
					settings_fields('zp-theme-options');

					do_settings_sections( 'zp-theme-options' );

					submit_button();
				?>
			</form>

		</div>
	<?php
}
