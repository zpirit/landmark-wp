// DOM READY
(function ($, root, undefined) {

	$(function () {

		'use strict';

		// publish post on F2 keypress
		jQuery(document).on('keyup', function(event) {
			// are we on post edit page
			if(window.location.href.match(/post\.php/)) {
				// is the F1 key pressed
				if( event.keyCode === 113 ) {
					jQuery("#publish").click();
				}
			}
		});

	});

})(jQuery, this);