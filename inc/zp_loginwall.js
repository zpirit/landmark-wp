
(function () {
	
	/* global grecaptcha, zpwall */

	var login_form = document.getElementById('loginform');
	if ( login_form ) {		
		
		// VARS
		var user_ip,
			user_weight,
			submit_btn_txt,
			submit_btn;
		
		// loop over form fields
		for ( var i = 0; i < login_form.length; i++ ) {
			var field = login_form[i];
			if ( field.name === "user-ip" ) 
				user_ip = field.value;
			
			else if ( field.name === "wp-submit" )
				submit_btn = field;

			else if ( field.name === "user-weight" )
				user_weight = field;
		}

		// store original btn value
		submit_btn_txt = submit_btn.value;
		// set to disabled
		submit_btn.disabled = "true";
		// short msg
		submit_btn.value = 'Vent litt..';

		// verify user with Google catpcha V3.
		grecaptcha.ready(function() {
			grecaptcha.execute(
				atob( zpwall.rand ), 
				{action: 'wplogin'}
			).then( function(token) {

				var query = "action=" + encodeURIComponent( 'zp_verify_gc' );
				query += "&response=" + encodeURIComponent( token );
				query += "&remoteip=" + encodeURIComponent( user_ip );
				query += "&veritas=" + encodeURIComponent( zpwall.veritas );

				var ajax = new XMLHttpRequest();
				ajax.open( 'POST', zpwall.ajax );
				ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
				ajax.onload = function() {
					// http response 200 ok
					if ( ajax.status === 200 ) {
						var result = JSON.parse(ajax.responseText);
						if ( result.hasOwnProperty('success') && result.success === true ) {
							if ( result.data.hasOwnProperty('score') ) {
								user_weight.value = result.data.score;
								submit_btn.disabled = false;
								submit_btn.value = submit_btn_txt;
							}
						}
						else {
							// ajax failed
							console.warn( "Contact devteam about Captcha" );
						}
					}
					else {
						// ajax failed
						console.warn( "Contact devteam about Captcha" );
					}
				};
	
				ajax.send( query );

			});
		});
	}


})();