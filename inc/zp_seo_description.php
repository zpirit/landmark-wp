<?php
/**
 *
 * Add description meta tag for each post or page
 *
 * Enjoy!!!
 *
 * @author Øyvind eikeland <oyvind@zpirit.no>
 * @since 1.0
 */

/**
 * Add meta box to page and post
 */
add_action( 'add_meta_boxes', 'zp_create_description_meta' );
add_action( 'save_post', 'zp_save_description_meta' );
function zp_create_description_meta() {
	add_meta_box( 
		'zp_post_description', 
		'For søkjemotorar', 
		'zp_show_description_meta', 
		array('post', 'nmec_project','page'), 
		'advanced' );
}
function zp_show_description_meta($post) {
	wp_nonce_field( 'zp_description_verify', 'zp_description_nonce');

	echo sprintf('<div><p><label for="%1$s">Utdrag til visning på søkjemotorar som Google.</label></p></p><textarea id="%1$s" name="%1$s" rows="5" cols="80">%2$s</textarea></p></div>', 'zp_seo_meta_description', esc_attr( get_post_meta( $post->ID, 'zp_seo_meta_description', true ) ) );

}
function zp_save_description_meta($post_id) {
	// Check if nonce is set.
	if( !isset( $_POST['zp_description_nonce'] ) ) {
		return $post_id;
	}
	// Verify nonce
	if( !wp_verify_nonce( $_POST['zp_description_nonce'], 'zp_description_verify' ) ) {
		return $post_id;
	}

    // to prevent metadata or custom fields from disappearing... 
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    	return $post_id;
	}

	// no action on revision
	if( wp_is_post_revision( $post_id ) ) {

	}
	if( in_array( $_POST['post_type'], array( 'post', 'page' ) ) ) {

		if( !current_user_can( 'edit_post', $post_id ) ) {
			return $post_id;
		}	
	}
	else {
		return $post_id;
	}

	// All good, Update meta values
	if( isset( $_POST['zp_seo_meta_description'] ) ) {
		update_post_meta( $post_id, 'zp_seo_meta_description', sanitize_text_field( $_POST['zp_seo_meta_description'] ) );
	}
	
}
/**
 * Displays custom meta description for page/post in head
 */
add_action('wp_head', 'zp_seo_description', 5);
function zp_seo_description() {
	global $post;

	if( is_single() || (is_page() && !is_front_page())) {
		// Get post custom field
		$desc_meta = get_post_meta( $post->ID, 'zp_seo_meta_description', true );
		if( !empty($desc_meta)) {
			echo sprintf('<meta name="description" content="%s" />%s', $desc_meta, "\n");
		}
		else {
			echo sprintf('<meta name="description" content="%s" />%s', get_bloginfo('description'), "\n");
		}
	}
	// Not page or single_post or main frontpage
	else {
		// Display global site description
		echo sprintf('<meta name="description" content="%s" />%s', get_bloginfo('description'), "\n");
	}
}
?>