<?php // The default template for displaying pages ?>

<?php get_header(); ?>

<?php 
// Define variables
$bento_parent_page_id = get_the_ID();
?>

<div class="bnt-container">
    
    <div class="content content-page">
        <main class="site-main">
        
            <?php 
            // Start the Loop
            if ( have_posts() ) { 
                while ( have_posts() ) { 
                    the_post(); 
                    // Include the page content
                    get_template_part( 'content', 'page' );   	

					// If comments are open or the page has at least one comment, load the comments template.
                    if ( comments_open() || get_comments_number() ) {
                        comments_template();
                    }
					
                // End the Loop
                } 
            }
            ?>
    



		<?php if ( is_page('15557')) : //kontaktside ?>
		
		
			<div id="kontaktpersoner-avdeling" class="kontaktpersoner-avdeling">
				
				<h2>Våre kontakter</h2>
				
				<?php 
					$args = array(
						'post_type' => 'ansatt',
						'posts_per_page' => 99,
						'orderby' => 'menu_order',
						'order' => 'ASC',
						
					);
					
					$query = new WP_Query( $args );
					if ( $query->have_posts() ) : 
						while ( $query->have_posts() ) : 
							$query->the_post(); ?>	
							<div id="<?php echo $post->post_name; ?>" class="kontaktperson-wrapper">
								<div class="kontaktperson-bilde">
									<?php
													// Must be inside a loop.
													 
													if ( has_post_thumbnail() ) {
													    echo sprintf( '%s', get_the_post_thumbnail( $post, 'employee-THUMB') );
													}
													else {
													    echo '<img src="' . get_stylesheet_directory_uri() . '/img/employee-landmark-default.png" />';
													}
													?>
								</div>
								<div class="kontaktperson-kontaktinformasjon">
									<h3 class="navn"><?php the_title(); ?></h3>
									<?php if( get_field('kontakt_tittel') ): ?>
									<p class="tittel"><strong><?php the_field('kontakt_tittel'); ?><?php if( get_field('kontakt_tittel_2') ): ?>
									<br/><?php the_field('kontakt_tittel_2'); ?><?php endif; ?></strong></p>
									<?php endif; ?>
									<?php if( get_field('kontakt_mobil') ): ?>
									<p><i class="fas fa-mobile"></i> +47 <?php the_field('kontakt_mobil'); ?></p>
									<?php endif; ?>
									<?php if( get_field('kontakt_telefon') ): ?>
									<p><i class="fas fa-phone"></i> +47 <?php the_field('kontakt_telefon'); ?></p>
									<?php endif; ?>
									<?php if( get_field('kontakt_epost') ): ?>
									<p><i class="fas fa-envelope"></i> <a href="mailto:<?php the_field('kontakt_epost'); ?>">Send e-post</a></p>
									<?php endif; ?>
								</div>
							</div>
						<?php 
						endwhile; 
						wp_reset_postdata();
					endif; 
				?>
			</div>

					<?php endif; ?>








			
			
			<?php if ( is_page('15589')) : //Tjenester ?>
			
			<?php if( have_rows('add_service_box') ): ?>

				<div class="servicebox_container">
			
				<?php while( have_rows('add_service_box') ): the_row(); 
			
					// vars
					$image = get_sub_field('service_image');
					$title = get_sub_field('service_title');
					$content = get_sub_field('service_content');
					$link = get_sub_field('service_link');
			
					?>
			
					<div class="servicebox_single" style="background-image: url(<?php echo $image['url']; ?>)">
			
						<?php if( $link ): ?>
							<a href="<?php echo $link; ?>">
						<?php endif; ?>	
						<div class="servicebox_content">
		
			

						<h3><?php echo $title; ?></h3>
					    <p><?php echo $content; ?></p>
						</div>
												<?php if( $link ): ?>
							</a>
						<?php endif; ?>
					</div>
			
				<?php endwhile; ?>
			
				</div>
			
			<?php endif; ?>
			
			<?php endif; ?>
			
			<?php if ( is_page('999999999')) : //Referanseside ID 15587 ?>
		
		
					
					
				<?php
							
							
				// Hent toppnivå kategoriar i employee_category
				$sub_terms = get_terms( 'referanser-segment', array( 'orderby' => 'name', 'hide_empty' => true ) );
				
				// Sorter foreldre kategoriar og legg dei til i nytt array
				$parents_sorted = array();
				$orders = array(51,52,53,54,55,56,57,58,59);
				foreach ($orders as $order) {
					foreach ($sub_terms as $term) {
						if($term->term_id == $order) {
							$sub_sorted[] = array('term_id' => $term->term_id, 'name' => $term->name, 'slug' => $term->slug, 'description' => $term->description);
						}
					}
				}
			
				foreach ($sub_sorted as $sub_area) : ?>

												<div class="entry-content referanser-sub-group container">
													<!--<a name="<?php echo $sub_area['slug']; ?>" class="contact-anchor">&nbsp;</a>-->
													<h2 class="showhide"><span><?php echo $sub_area['name']; ?></span></h2>
													<?php if(trim($sub_area['description']) != "") : ?>
													<p class="department-address"><?php echo '<i class="fa fa-map-marker" aria-hidden="true"></i>' . __( $sub_area['description']); ?></p>
													<?php endif; ?>
													
													<div class="content <?php echo $sub_area->slug; ?>">
														<?php // WP_Query arguments
														$subargs = array (
															'post_type'              => 'lm-referanser',
															'posts_per_page'         => '999',
															'orderby' 				 => 'menu_order date', 
															'order' 				 => 'ASC',
															'tax_query'              => array(
																							array(
																								'taxonomy' => 'referanser-segment',
																								'field'    => 'term_id',
																								'terms'    => $sub_area['term_id'],
																							),
															)
														);
														
														// The Query
														$children = new WP_Query( $subargs );
														
														// The Loop
														if($children->have_posts()) : while($children->have_posts()) : $children->the_post(); ?>

																	
															<article id="post-<?php the_ID(); ?>" <?php post_class('grid25'); ?>>
															<?php 
																if( has_post_thumbnail() ) { 
																	$image = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'fivexthree');
																	$imagefull = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'full');
																	echo sprintf('<a href="%s" rel="lightbox" data-rel="lightbox" data-lightbox-gallery="lightbox-gallery"><img src="%s" alt="%s" title="%s"></a>', $imagefull[0], $image[0], get_the_title(), get_the_title());
																} 
															?>
																	
																	<h3><?php the_title(); ?></h3>
																	
															</article>

																	<?php if(($children->current_post + 1) % 4 == 0) echo '<div class="clear"></div>'; ?>

															<?php endwhile; ?>
														<?php else: ?>


														<?php endif; ?>
														
														<?php wp_reset_postdata(); ?>
													</div>
													
													
												</div><!-- End .employee-sub-group -->

											<?php endforeach; // End foreach sub term ?>

							<?php endif; ?>



    
        </main>
    </div>
    
    <?php get_sidebar(); ?>
    
</div>

<?php get_footer(); ?>