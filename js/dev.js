(function ($, root, undefined) {
	
	$(function () {
		
		'use strict';
		
		// Check if browser supports SVG elements ftw
		function supportsSvg() {
			return !! document.createElementNS && !! document.createElementNS('http://www.w3.org/2000/svg','svg').createSVGRect;
		}
		if(!supportsSvg()) {
			$('html').addClass('no-svg');
			// no svg support replace svg images with png
			$('img[src*="svg"]').attr('src', function() {
				return $(this).attr('src').replace('.svg', '.png');
			});
		}
		
		
	});
	
	jQuery(document).ready(function() {
	    jQuery('#frontpageslider').flexslider({
				animation: "fade", 
				useCSS: false, 
				slideshow: true, 
				animationLoop: true,
				slideshowSpeed: 5000, 
				animationSpeed: 400, 
				initDelay: 0, 
				pauseOnHover: false, 
				directionNav: false, 
				prevText: "", 
				nextText: "", 
				touch: false,
				controlNav: false,
				// start: function(){}, 
				// before: function(){}, 
				// after: function(){}, 
				// end: function(){}
			});
	});
	

$(".showhide").click(function () {
	$(this).toggleClass("active");
	
	
    $header = $(this);
    //getting the next element
    $content = $header.next();
    //open up the content needed - toggle the slide- if visible, slide up, if not slidedown.
    $content.slideToggle(400, function () {
        //execute this after slideToggle is done
        //change text of header based on visibility of content div
    });

});


})(jQuery, this);
