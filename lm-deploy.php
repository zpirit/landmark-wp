<?php
/**
 * Deploy git repo with webhookses
 *
 * @author �yvind Eikeland <oyvind@zpirit.no>
 * @version 1.2
 * 
 * - Notice V 1.2
 * 			-- Getting path to git with which command if git is not in $PATH *
 * - Notice V 1.1
 * 			-- Setting 404 error if this is not a bitbucket request.
 */

// Check if this is a post request from bitbucket webhooks
if( $_SERVER['REQUEST_METHOD'] === "POST" && $_SERVER['HTTP_USER_AGENT'] === "Bitbucket-Webhooks/2.0") {
	// repo variables
	$gitrepo = "landmark-wp";
	$allowed_users = array('oyvindeikeland', 'espenkvalheim');

	// request data from bitbucket
	$data = json_decode(file_get_contents("php://input"));

	// Correct repo
	if( $data->repository->name === $gitrepo ) {
		// If branch is master
		if( $data->push->changes[0]->new->name === "master" ) {
			// only deploy if user is allowed
			if ( in_array( $data->actor->username, $allowed_users ) ) {
				$output_git = shell_exec( 'git pull origin master' );
				if( $output_git !== NULL ) {
					echo "Repo updated.";
				}
				else {
					// Try git with full path
					$output_git = shell_exec( '/usr/local/cpanel/3rdparty/lib/path-bin/git pull origin master' );
					if( $output_git !== NULL ) {
						echo "Repo updated.";
					}
					else {
						echo "Repo is not updating, check if required php functions are activated.";
					}
				}
			}
			else {
				echo "Only allowed users can deploy this repo.";
			}
		}
		else {
			echo "Only master branch updates are accepted.";
		}
	}
	else {
		echo "Wrong repository, check setting \$gitrepo.";
	}
}
else {
	// set 404 error and the file to show
	http_response_code(404);
	die();
}

// When all is said and done.
exit();

?>
