var gulp = require('gulp');
var postcss = require('gulp-postcss');
var sass = require('gulp-sass');
var autoprefixer = require('autoprefixer');
var cssnano = require('cssnano');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');

gulp.task('style', function(){
	var processors = [
		autoprefixer({browsers: ['last 3 versions']}),
		cssnano
	];
	return gulp.src('./style.scss')
		.pipe(sass({
			style: 'expanded', 
			sourcemap: true 
		}).on('error', sass.logError))
		.pipe(postcss(processors))
		.pipe(gulp.dest('./'));
});

gulp.task('script', function() {
	return gulp.src(['js/dev.js', 'js/libs/flexslider-2.6.3/jquery.flexslider-min.js', 'js/libs/jquery.fitvids.js', 'js/jquery.js' ])
	.pipe(concat('script.min.js'))
	.pipe(uglify())
	.pipe(gulp.dest('./js/'));
});

gulp.task('watch', function() {
	gulp.watch('./style.scss', ['style']);
	gulp.watch('js/dev.js', ['script']);
});